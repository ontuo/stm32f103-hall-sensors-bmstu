#define TIME_DELAY 1
#define STATE_1 0b000100
#define STATE_2 0b000110
#define STATE_3 0b000010
#define STATE_4 0b000011
#define STATE_5 0b000001
#define STATE_6 0b000101

void setup() {
 DDRB =  0b000111;
 PORTB = 0b000000;
}

void loop() {
  PORTB = STATE_1;
  delay(TIME_DELAY);

  PORTB = STATE_2;
  delay(TIME_DELAY);

  PORTB = STATE_3;
  delay(TIME_DELAY);

  PORTB = STATE_4;
  delay(TIME_DELAY);

  PORTB = STATE_5;
  delay(TIME_DELAY);

  PORTB = STATE_6;
  delay(TIME_DELAY);

  
}
