# Getting data from Hall sensors

Project for Bauman Robotics that reads data from Hall sensors, converts it into speed and push it to the higher level. 

## Getting started

This project works with 3 Hall sensors and STM32f103. Sensors are connected to PA1, PA2, Pa3 pins, UART uses PA10 (Rx) and PA9 (Tx). To start this project you need:
1. STM32CubeMX to start .ioc file and reconfigurate something if you needed.
2. Atollic TrueSTUDIO for STM32.

## Testing without Hall sensors

The project contains the simulate_encoder folder. There is a sketch for Arduino Uno that simulates the readings from sensors. To test you should connect Arduino digital pins 8, 9, 10 to STM32 pins PA1, PA2, PA3 (dont forget to connect ground pins).


